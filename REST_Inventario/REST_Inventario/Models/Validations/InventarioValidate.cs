﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_Inventario.Models
{
    [MetadataType(typeof(Inventario.MetaData))]
    public partial class Inventario
    {
        sealed class MetaData
        {
            [Key]
            public int Invid;

            [Required(ErrorMessage = "Ingresa el nombre del Objeto")]
            public string Invname;

            [Required(ErrorMessage = "Ingresa caracteristicas del Objeto")]
            public string Carac;

            [Required]
            [Range(2000, 10000, ErrorMessage = "Edad entre 2000 y 10000")]
            public Nullable<int> Precio;
        }
    }
}