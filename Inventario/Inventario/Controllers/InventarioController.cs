﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventario.Models;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Controllers
{
    public class InventarioController : Controller
    {
        private readonly ApplicationDbContext _db;

        public InventarioController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var displaydata = _db.Inventario.ToList();
            return View(displaydata);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(InventarioModel nEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Inventario.FindAsync(id);
            return View(getEmpDdetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Inventario.FindAsync(id);
            return View(getEmpDdetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(InventarioModel oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDdetail = await _db.Inventario.FindAsync(id);
            return View(getEmpDdetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDdetail = await _db.Inventario.FindAsync(id);
            _db.Inventario.Remove(getEmpDdetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
