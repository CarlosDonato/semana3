﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventario.Models
{
    public class InventarioModel
    {
        [Key]
        public int Invid { get; set; }

        [Required(ErrorMessage = "Ingresa Nombre del objeto")]
        [Display(Name = "Nombre de Objeto")]
        public string Invname { get; set; }

        [Required(ErrorMessage = "Ingresa caracteristicas del objeto")]
        [Display(Name = "Caracteristicas")]
        public string Carac { get; set; }

        [Required(ErrorMessage = "Fije un Precio")]
        [Display(Name = "Precio")]
        public int Precio { get; set; }
    }
}
